+++
title = "Sid, on the Net"
+++

# Hi, it's Sid!

## Henlo welcome to my corner of the internet
I’m a Physics graduate with a keen interest in Computer Science. This is my personal space where I share my journey, projects, and thoughts on everything that crosses my mind.

## Look at what I've done!
You can find my contributions at:
- [Freedesktop's GitLab](https://gitlab.freedesktop.org/Sid127)
- [My own GitLab](https://gitlab.com/Sid127)
- [My mostly abandoned Github](https://github.com/Sid127)
- [My XDA Profile](https://xdaforums.com/m/sid127.7856520/)
- [The Linux kernel?](https://github.com/torvalds/linux/commit/f6ecfdad359a01c7fd8a3bcfde3ef0acdf107e6e)

## Wanna know more?
You could head to my ["About Me"](./about) page, where I also have contact details, or maybe read up some of my [blog posts](./blog)?
