+++
title = "Sid, on the Net"
+++

# Hi, it's Sid!

## Henlo welcome to my corner of the internet
I'm just a silly bean trying to dump some thoughts and share some adventures, and I'm glad you are curious enough (or maybe love me enough?) to come check out what I'm up to :>

## Why does this exist?
I bought this domain both because I didn't like the one I had before this, and partly as a joke.
Over time this stupid domain name grew on me, because I ended up enjoying the silly things I could do with it.

I often embark on stupid adventures, both with tech and in real life, and I wanted a way to share them with the world without blowing up my friends' DMs one person at a time. Plus here I don't have the guilt of potentially bothering anyone with my silly stories.

## Wanna know more?
You could head to my ["About Me"](./about) page, or maybe read up some of my [blog posts](./blog)?