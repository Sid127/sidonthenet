+++
title = "About"
+++
Hi, I'm Sid, nice to meet you. I go by a couple more names - Chiku and onebigsucc (a name I've left behind in most places).

I'm a `FOSS, security, and privacy enthusiast`, and am semi-competent with `Shell scripting` and `Javascript`, and can manage to cough up some code in C, C++, or virtually any language required given the time to figure out how to apply my logic using the tools the language provides me with. I also enjoy tinkering with things, whether it be software or hardware, and can somewhat identify sources of error in software. 

I have, for fun, dabbled in building my own `Human Interface Device out of an RP2040`, `modded my UEFI firmware`, `written a discord bot`, maintained various `custom ROMs` for my old android phone, `ported a few mobile linux OSs` to the same phone, tried my hand at `learning how to mainline` said phone, among a few more things I think I'm forgetting.

Funnily enough, despite my knowledge and "prowess" with computers, I'm a `physics major`. I believe in learning by doing, and having a better understanding of the world around me through experimental physics is the career path I'd like to trod down. Maybe that'll change in the future, who knows :>

My other interests/hobbies include, but are not limited to, 
- `music`: I've been playing the cajón since the age of 14 and am learning the drums. I listen to most genres of music, but tend to lean towards metal, classic rock, and blues.
- `rally`: the only motorsport I find fun to watch, and an interest I'd like to turn into a hobby
- `video games`: retro shooters first and foremost, but also rally simulators, strategy games, fighting games, and cozy games
- `biking`: born to bike, forced to walk/take the transit
- `cooking`: I aspire to have a little kitchen garden and grow my own ingredients someday
- `travel`: relatively new thing I've picked up, but seeing different cultures and just finding places to relax is fun
- `making connections` with new people, despite not being very good at the whole socializing shtick
- `obscure/quirky tech`: I think this is a bit self explanatory (:

## Oh you wanna reach me elsewhere?
Well,
- You could [mail@sidonthe.net](mailto:mail@sidonthe.net)
- Or you could [@text:sidonthe.net](https://matrix.to/#/@text:sidonthe.net)
- *to share or not to share, that is the question...*